import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


# CREATE "POST" LOCATION picture using PEXELS API
def get_photo(city, state):
    # Define URL
    url = 'https://api.pexels.com/v1/search'

    # Define headers to use your 'unique' API key
    headers = {"Authorization": PEXELS_API_KEY}

    # Define your parameters
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }

    # Define your request with above variables
    photo = requests.get(url, params=params, headers=headers)

    # Convert JSON str to PYTHON dict
    content = json.loads(photo.content)
    # Return the content dict
    return {"picture_url": content["photos"][0]["url"]}


def get_weather_data(city, state):
    # Define URL
    url_lat_long = 'http://api.openweathermap.org/geo/1.0/direct'
    url_weather_data = 'https://api.openweathermap.org/data/2.5/weather'

    # Define headers
    headers = {"Authorization": OPEN_WEATHER_API_KEY}

    # Define parameters for lat/lon request
    location_params = {
        "q": city + "," + state + ",",
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1,
    }

    # request for lat/lon values
    r = requests.get(url_lat_long, params=location_params, headers=headers)

    # convert JSON str into python values
    content = json.loads(r.content)
    lat = content[0]["lat"]
    lon = content[0]["lon"]

    # Define parameters for temp request
    weather_params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
    }

    # request for temp value
    r2 = requests.get(url_weather_data, params=weather_params, headers=headers)

    # convert JSON str into python valu
    content2 = json.loads(r2.content)
    temp_kelvin = content2["main"]["temp"]
    temp_fahrenheit = 9/5 * (temp_kelvin - 273) + 32

    # create weather_data dict
    weather_data = {}
    weather_data["temperature"] = temp_fahrenheit
    weather_data["description"] = content2["weather"][0]["description"]

    # return created weather_data dict
    return weather_data
